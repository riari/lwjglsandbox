package terrain;

import org.lwjgl.util.Point;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TerrainTileSet {
    private Map<Point, TerrainTile> tiles = new HashMap<>();

    public void add(TerrainTile tile) {
        tiles.put(new Point(tile.getGridX(), tile.getGridZ()), tile);
    }

    public TerrainTile getTileAt(float worldX, float worldZ) {
        int x = (int) (worldX / TerrainTile.SIZE);
        int z = (int) (worldZ / TerrainTile.SIZE);

        return tiles.get(new Point(x, z));
    }

    public float getHeightAt(float worldX, float worldZ) {
        TerrainTile tile = getTileAt(worldX, worldZ);
        return tile.getHeightAt(worldX, worldZ);
    }

    public ArrayList<TerrainTile> toList() {
        return new ArrayList<>(tiles.values());
    }
}
