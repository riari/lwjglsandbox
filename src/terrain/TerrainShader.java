package terrain;

import entities.Camera;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import shaders.ShaderProgram;
import utils.Maths;

public class TerrainShader extends ShaderProgram {
    private static final String VERTEX_SRC_FILE = "TerrainVS";
    private static final String FRAGMENT_SRC_FILE = "TerrainFS";

    private int location_transformationMatrix;

    private int location_projectionMatrix;

    private int location_viewMatrix;

    private int location_toShadowMapSpace;
    private int location_shadowDistance;
    private int location_shadowMap;
    private int location_shadowMapSize;

    private int location_clippingPlane;

    private int location_backgroundTexture;
    private int location_rTexture;
    private int location_gTexture;
    private int location_bTexture;
    private int location_blendMap;

    private int location_skyColour;

    private int location_shineDamper;
    private int location_reflectivity;

    public TerrainShader() {
        super(VERTEX_SRC_FILE, FRAGMENT_SRC_FILE);
    }

    @Override
    protected void bindAttributes() {
        bindAttribute(0, "position");
        bindAttribute(1, "textureCoords");
        bindAttribute(2, "normal");
    }

    @Override
    protected void getAllUniformLocations() {
        location_transformationMatrix = getUniformLocation("transformationMatrix");

        location_projectionMatrix = getUniformLocation("projectionMatrix");

        location_viewMatrix = getUniformLocation("viewMatrix");

        location_toShadowMapSpace = getUniformLocation("toShadowMapSpace");
        location_shadowDistance = getUniformLocation("shadowDistance");
        location_shadowMap = getUniformLocation("shadowMap");
        location_shadowMapSize = getUniformLocation("shadowMapSize");

        location_clippingPlane = getUniformLocation("clippingPlane");

        location_backgroundTexture = getUniformLocation("backgroundTexture");
        location_rTexture = getUniformLocation("rTexture");
        location_gTexture = getUniformLocation("gTexture");
        location_bTexture = getUniformLocation("bTexture");
        location_blendMap = getUniformLocation("blendMap");

        location_lightPosition = new int[MAX_LIGHTS];
        location_lightColour = new int[MAX_LIGHTS];
        location_lightAttenuation = new int[MAX_LIGHTS];
        for (int i = 0; i < MAX_LIGHTS; i++) {
            location_lightPosition[i] = getUniformLocation("lightPosition[" + i + "]");
            location_lightColour[i] = getUniformLocation("lightColour[" + i + "]");
            location_lightAttenuation[i] = getUniformLocation("lightAttenuation[" + i + "]");
        }

        location_skyColour = getUniformLocation("skyColour");

        location_shineDamper = getUniformLocation("shineDamper");
        location_reflectivity = getUniformLocation("reflectivity");
    }

    public void loadTransformationMatrix(Matrix4f matrix) {
        loadMatrix(location_transformationMatrix, matrix);
    }

    public void loadProjectionMatrix(Matrix4f projection) {
        loadMatrix(location_projectionMatrix, projection);
    }

    public void loadViewMatrix(Camera camera) {
        Matrix4f viewMatrix = Maths.createViewMatrix(camera);
        loadMatrix(location_viewMatrix, viewMatrix);
    }

    public void loadToShadowSpaceMatrix(Matrix4f matrix) {
        loadMatrix(location_toShadowMapSpace, matrix);
    }

    public void loadShadowDistance(float distance) {
        loadFloat(location_shadowDistance, distance);
    }

    public void loadShadowMapSize(float size) {
        loadFloat(location_shadowMapSize, size);
    }

    public void loadClippingPlane(Vector4f plane) {
        load4DVector(location_clippingPlane, plane);
    }

    public void connectTextureUnits() {
        loadInt(location_backgroundTexture, 0);
        loadInt(location_rTexture, 1);
        loadInt(location_gTexture, 2);
        loadInt(location_bTexture, 3);
        loadInt(location_blendMap, 4);
        loadInt(location_shadowMap, 5);
    }

    public void loadSkyColour(Vector3f colour) {
        load3DVector(location_skyColour, colour);
    }

    public void loadShine(float damper, float reflectivity) {
        loadFloat(location_shineDamper, damper);
        loadFloat(location_reflectivity, reflectivity);
    }
}
