package terrain;

import models.RawModel;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import renderEngine.Loader;
import textures.TerrainTexture;
import textures.TerrainTexturePack;
import utils.Maths;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

public class TerrainTile {
    public static final float SIZE = 800;
    private static final int VERTEX_COUNT = 256;
    private static final int SEED = new Random().nextInt(10000000);

    private int gridX;
    private int gridZ;
    private float x;
    private float z;
    private TerrainTexturePack texturePack;
    private TerrainTexture blendMap;
    private HeightsGenerator generator;
    private RawModel model;

    private float[][] heights;

    public TerrainTile(int gridX, int gridZ, Loader loader, TerrainTexturePack texturePack, TerrainTexture blendMap) {
        this.gridX = gridX;
        this.gridZ = gridZ;
        this.x = gridX * SIZE;
        this.z = gridZ * SIZE;
        this.texturePack = texturePack;
        this.blendMap = blendMap;
        this.generator = new HeightsGenerator(gridX, gridZ, VERTEX_COUNT, SEED);
        this.model = generateTerrain(loader);
    }

    public int getGridX() {
        return gridX;
    }

    public int getGridZ() {
        return gridZ;
    }

    public float getX() {
        return x;
    }

    public float getZ() {
        return z;
    }

    public RawModel getModel() {
        return model;
    }

    public TerrainTexturePack getTexturePack() {
        return texturePack;
    }

    public TerrainTexture getBlendMap() {
        return blendMap;
    }

    public float getHeightAt(float worldX, float worldZ) {
        float terrainX = worldX - this.x;
        float terrainZ = worldZ - this.z;
        float gridSquareSize = SIZE / ((float) heights.length - 1);
        int heightGridX = (int) Math.floor(terrainX / gridSquareSize);
        int heightGridZ = (int) Math.floor(terrainZ / gridSquareSize);

        if (heightGridX >= heights.length - 1 || heightGridZ >= heights.length - 1 || heightGridX < 0 || heightGridZ < 0) {
            return 0;
        }

        float xCoord = (terrainX % gridSquareSize) / gridSquareSize;
        float zCoord = (terrainZ % gridSquareSize) / gridSquareSize;

        if (xCoord <= (1 - zCoord)) {
            return Maths.baryCentric(new Vector3f(0, heights[heightGridX][heightGridZ], 0), new Vector3f(1,
                            heights[heightGridX + 1][heightGridZ], 0), new Vector3f(0,
                            heights[heightGridX][heightGridZ + 1], 1), new Vector2f(xCoord, zCoord));
        }

        return Maths.baryCentric(new Vector3f(1, heights[heightGridX + 1][heightGridZ], 0), new Vector3f(1,
                heights[heightGridX + 1][heightGridZ + 1], 1), new Vector3f(0,
                heights[heightGridX][heightGridZ + 1], 1), new Vector2f(xCoord, zCoord));
    }

    private RawModel generateTerrain(Loader loader) {
        heights = new float[VERTEX_COUNT][VERTEX_COUNT];

        int count = VERTEX_COUNT * VERTEX_COUNT;
        float[] vertices = new float[count * 3];
        float[] normals = new float[count * 3];
        float[] textureCoords = new float[count * 2];
        int[] indices = new int[6 * (VERTEX_COUNT - 1) * (VERTEX_COUNT - 1)];
        int vertexPointer = 0;

        for (int i = 0; i < VERTEX_COUNT; i++) {
            for (int j = 0; j < VERTEX_COUNT; j++) {
                vertices[vertexPointer * 3] = (float) j / ((float) VERTEX_COUNT - 1) * SIZE;
                float height = getMapHeightAt(j, i);
                heights[j][i] = height;
                vertices[vertexPointer * 3 + 1] = height;
                vertices[vertexPointer * 3 + 2] = (float) i / ((float) VERTEX_COUNT - 1) * SIZE;
                Vector3f normal = getMapNormalAt(j, i);
                normals[vertexPointer * 3] = normal.x;
                normals[vertexPointer * 3 + 1] = normal.y;
                normals[vertexPointer * 3 + 2] = normal.z;
                textureCoords[vertexPointer * 2] = (float) j / ((float) VERTEX_COUNT - 1);
                textureCoords[vertexPointer * 2 + 1] = (float) i / ((float) VERTEX_COUNT - 1);
                vertexPointer++;
            }
        }

        int pointer = 0;
        for (int gz = 0; gz < VERTEX_COUNT - 1; gz++) {
            for (int gx = 0; gx < VERTEX_COUNT - 1; gx++) {
                int topLeft = (gz * VERTEX_COUNT) + gx;
                int topRight = topLeft + 1;
                int bottomLeft = ((gz + 1) * VERTEX_COUNT) + gx;
                int bottomRight = bottomLeft + 1;
                indices[pointer++] = topLeft;
                indices[pointer++] = bottomLeft;
                indices[pointer++] = topRight;
                indices[pointer++] = topRight;
                indices[pointer++] = bottomLeft;
                indices[pointer++] = bottomRight;
            }
        }

        return loader.initialiseVAO(vertices, textureCoords, normals, indices);
    }

    private Vector3f getMapNormalAt(int x, int z) {
        float heightL = getMapHeightAt(x - 1, z);
        float heightR = getMapHeightAt(x + 1, z);
        float heightD = getMapHeightAt(x, z - 1);
        float heightU = getMapHeightAt(x, z + 1);

        Vector3f normal = new Vector3f(heightL - heightR, 2f, heightD - heightU);
        normal.normalise();

        return normal;
    }

    private float getMapHeightAt(int x, int z) {
        return generator.generateHeight(x, z);
    }
}
