#version 400 core

in vec4 clipSpace;
in vec2 textureCoords;
in vec3 toCameraVector;
in vec3 fromLightVector;

out vec4 out_Color;

uniform sampler2D reflectionTexture;
uniform sampler2D refractionTexture;
uniform sampler2D dudvMap;
uniform sampler2D normalMap;
uniform sampler2D depthMap;
uniform vec3 lightColour;
uniform float moveFactor;

const float waveStrength = 0.04;
const float shineDamper = 40.0;
const float reflectivity = 0.4;

void main(void) {
    vec2 ndc = (clipSpace.xy / clipSpace.w) / 2.0 + 0.5;

    vec2 reflectionTextureCoords = vec2(ndc.x, -ndc.y);
    vec2 refractionTextureCoords = vec2(ndc.x, ndc.y);

    // TODO: use uniforms for these
    float near = 0.1;
    float far = 1000.0f;
    float depth = 2.0 * texture(depthMap, refractionTextureCoords).r - 1.0;
    float floorDistance = 2.0 * near * far / (far + near - depth * (far - near));
    depth = 2.0 * gl_FragCoord.z - 1.0;
    float waterDistance = 2.0 * near * far / (far + near - depth * (far - near));
    float waterDepth = floorDistance - waterDistance;

    vec2 distortedTexCoords = texture(dudvMap, vec2(textureCoords.x + moveFactor, textureCoords.y)).rg * 0.1;
    distortedTexCoords = textureCoords + vec2(distortedTexCoords.x, distortedTexCoords.y + moveFactor);
    vec2 totalDistortion = (texture(dudvMap, distortedTexCoords).rg * 2.0 - 1.0) * waveStrength * clamp(waterDepth / 20.0, 0.0, 1.0);

    reflectionTextureCoords += totalDistortion;
    reflectionTextureCoords.x = clamp(reflectionTextureCoords.x, 0.001, 0.999);
    reflectionTextureCoords.y = clamp(reflectionTextureCoords.y, -0.999, -0.001);
    vec4 reflectionColour = texture(reflectionTexture, reflectionTextureCoords);

    refractionTextureCoords += totalDistortion;
    refractionTextureCoords = clamp(refractionTextureCoords, 0.001, 0.999);
    vec4 refractionColour = texture(refractionTexture, refractionTextureCoords);
    vec4 murkyWaterColour = vec4(0.2, 0.2, 0.0, 0.2);
    refractionColour = mix(refractionColour, murkyWaterColour, clamp(waterDepth / 60.0, 0.0, 1.0));

    vec4 normalMapColour = texture(normalMap, distortedTexCoords);
    vec3 normal = vec3(normalMapColour.r * 2.0 - 1.0, normalMapColour.b * 3.0, normalMapColour.g * 2.0 - 1.0);
    normal = normalize(normal);

    vec3 viewVector = normalize(toCameraVector);
    float refractiveFactor = dot(viewVector, normal);
    refractiveFactor = clamp(refractiveFactor, 0.001, 0.999);

    vec3 reflectedLight = reflect(normalize(fromLightVector), normal);
    float specular = max(dot(reflectedLight, viewVector), 0.0);
    specular = pow(specular, shineDamper);
    vec3 specularHighlights = lightColour * specular * reflectivity * clamp(waterDepth / 5.0, 0.0, 1.0);

    out_Color = mix(reflectionColour, refractionColour, refractiveFactor);
    out_Color = mix(out_Color, vec4(0.2, 0.2, 0.5, 1.0), 0.3) + vec4(specularHighlights, 0.0);
    out_Color.a = clamp(waterDepth / 5.0, 0.0, 1.0);
}