#version 330

uniform vec3 colour;
uniform sampler2D fontAtlas;

in vec2 pass_textureCoords;

out vec4 out_Colour;

const float smoothing = 1.0/16.0;
const float outlineDistance = 0.2;
const vec4 outlineColor = vec4(0, 0, 0, 1);

void main() {
//    float r = texture(fontAtlas, pass_textureCoords).r;
//
//    float scale = 1.0 / fwidth(r);
//    float signedDistance = (r - 0.5) * scale;
//
//    float brightness = clamp(signedDistance + 0.5, 0.0, 1.0);
//
//    if (brightness < 0.2) {
//        float alpha = clamp(signedDistance + 0.5 + scale * 0.125, 0.0, 1.0);
//        out_Colour = vec4(brightness, brightness, brightness, 1) * alpha;
//    } else {
//        out_Colour = vec4(brightness * colour.r, brightness * colour.g, brightness * colour.b, 1);
//    }

    float distance = texture(fontAtlas, pass_textureCoords).r;
    float outlineFactor = smoothstep(0.5 - smoothing, 0.5 + smoothing, distance);
    vec4 color = mix(outlineColor, vec4(colour, 1), outlineFactor);
    float alpha = smoothstep(outlineDistance - smoothing, outlineDistance + smoothing, distance);
    out_Colour = vec4(color.rgb, color.a * alpha);
}