#version 400 core

in vec2 pass_textureCoordinates;
in vec3 surfaceNormal;
in vec3 toLightVector[5];
in vec3 toCameraVector;
in float visibility;

out vec4 out_Color;

uniform sampler2D modelTexture;
uniform vec3 lightColour[5];
uniform vec3 lightAttenuation[5];
uniform float shineDamper;
uniform float reflectivity;
uniform vec3 skyColour;
uniform vec3 tintColour;

const float shadingLevels = 3.0;

void main(void) {
    vec4 textureColour = texture(modelTexture, pass_textureCoordinates);
    if (textureColour.a < 0.5) {
        discard;
    }

    vec3 unitNormal = normalize(surfaceNormal);
    vec3 unitVectorToCamera = normalize(toCameraVector);

    vec3 totalDiffuse = vec3(0.0);
    vec3 totalSpecular = vec3(0.0);

    for (int i = 0; i < 5; i++) {
        float distance = length(toLightVector[i]);
        float attenuationFactor = lightAttenuation[i].x + (lightAttenuation[i].y * distance) + (lightAttenuation[i].z * distance * distance);

        vec3 unitLightVector = normalize(toLightVector[i]);
        float nDotl = dot(unitNormal, unitLightVector);
        float brightness = max(nDotl, 0.0);
//        float level = floor(brightness * shadingLevels);
//        brightness = level / shadingLevels;
        vec3 lightDirection = -unitLightVector;
        vec3 reflectedLightDirection = reflect(lightDirection, unitNormal);
        float specularFactor = dot(reflectedLightDirection, unitVectorToCamera);
        specularFactor = max(specularFactor, 0.0);
        float dampedFactor = pow(specularFactor, shineDamper);
//        level = floor(dampedFactor * shadingLevels);
//        dampedFactor = level / shadingLevels;

        totalDiffuse = totalDiffuse + (brightness * lightColour[i]) / attenuationFactor;
        totalSpecular = totalSpecular + (dampedFactor * reflectivity * lightColour[i]) / attenuationFactor;
    }

    totalDiffuse = max(totalDiffuse, 0.2);

    out_Color = vec4(totalDiffuse, 1.0) * texture(modelTexture, pass_textureCoordinates) + vec4(totalSpecular, 1.0) + vec4(tintColour, 1.0);
    out_Color = mix(vec4(skyColour, 1.0), out_Color, visibility);
}