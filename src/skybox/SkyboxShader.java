package skybox;

import org.lwjgl.util.vector.Matrix4f;

import entities.Camera;

import org.lwjgl.util.vector.Vector3f;
import renderEngine.DisplayManager;
import shaders.ShaderProgram;
import utils.Maths;

public class SkyboxShader extends ShaderProgram {
    private static final String VERTEX_FILE = "SkyboxVS";
    private static final String FRAGMENT_FILE = "SkyboxFS";

    private int location_projectionMatrix;
    private int location_viewMatrix;
    private int location_fogColour;
    private int location_cubeMap1;
    private int location_cubeMap2;
    private int location_blendFactor;

    private static final float ROTATION_SPEED = .2f;
    private float rotation = 0;

    public SkyboxShader() {
        super(VERTEX_FILE, FRAGMENT_FILE);
    }

    public void loadProjectionMatrix(Matrix4f matrix) {
        loadMatrix(location_projectionMatrix, matrix);
    }

    public void loadViewMatrix(Camera camera) {
        Matrix4f matrix = Maths.createViewMatrix(camera);
        matrix.m30 = 0;
        matrix.m31 = 0;
        matrix.m32 = 0;
        rotation += ROTATION_SPEED * DisplayManager.getFrameTimeSeconds();
        matrix.rotate((float) Math.toRadians(rotation), new Vector3f(0, 1, 0));
        loadMatrix(location_viewMatrix, matrix);
    }

    public void loadFogColour(Vector3f colour) {
        load3DVector(location_fogColour, colour);
    }

    public void connectTextureUnits() {
        loadInt(location_cubeMap1, 0);
        loadInt(location_cubeMap2, 1);
    }

    public void loadBlendFactor(float factor) {
        loadFloat(location_blendFactor, factor);
    }

    @Override
    protected void getAllUniformLocations() {
        location_projectionMatrix = getUniformLocation("projectionMatrix");
        location_viewMatrix = getUniformLocation("viewMatrix");
        location_fogColour = getUniformLocation("fogColour");
        location_cubeMap1 = getUniformLocation("cubeMap1");
        location_cubeMap2 = getUniformLocation("cubeMap2");
        location_blendFactor = getUniformLocation("blendFactor");
    }

    @Override
    protected void bindAttributes() {
        super.bindAttribute(0, "position");
    }
}