package postProcessing.effects;

import shaders.ShaderProgram;

public class ContrastShader extends ShaderProgram {
	private static final String VERTEX_FILE = "post/ContrastVS";
	private static final String FRAGMENT_FILE = "post/ContrastFS";
	
	public ContrastShader() {
		super(VERTEX_FILE, FRAGMENT_FILE);
	}

	@Override
	protected void getAllUniformLocations() {	
	}

	@Override
	protected void bindAttributes() {
		super.bindAttribute(0, "position");
	}
}
