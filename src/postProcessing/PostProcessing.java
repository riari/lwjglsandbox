package postProcessing;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import models.RawModel;
import postProcessing.effects.ContrastEffect;
import postProcessing.effects.HorizontalBlurEffect;
import postProcessing.effects.VerticalBlurEffect;
import renderEngine.Loader;

public class PostProcessing {
	private static final float[] POSITIONS = { -1, 1, -1, -1, 1, 1, 1, -1 };
	private static RawModel quad;
	private static ContrastEffect contrastEffect;
	private static HorizontalBlurEffect hBlurEffect;
	private static VerticalBlurEffect vBlurEffect;

	public static void init(Loader loader) {
		quad = loader.initialiseVAO(POSITIONS, 2);
		contrastEffect = new ContrastEffect();
		hBlurEffect = new HorizontalBlurEffect(Display.getWidth() / 4, Display.getHeight() / 4);
		vBlurEffect = new VerticalBlurEffect(Display.getWidth() / 4, Display.getHeight() / 4);
	}
	
	public static void doPostProcessing(int colourTexture) {
		start();
		hBlurEffect.render(colourTexture);
		vBlurEffect.render(hBlurEffect.getOutputTexture());
		contrastEffect.render(vBlurEffect.getOutputTexture());
		end();
	}
	
	public static void cleanUp() {
		contrastEffect.cleanUp();
		hBlurEffect.cleanUp();
		vBlurEffect.cleanUp();
	}
	
	private static void start() {
		GL30.glBindVertexArray(quad.getVaoID());
		GL20.glEnableVertexAttribArray(0);
		GL11.glDisable(GL11.GL_DEPTH_TEST);
	}
	
	private static void end() {
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL20.glDisableVertexAttribArray(0);
		GL30.glBindVertexArray(0);
	}
}
