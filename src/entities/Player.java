package entities;

import models.TexturedModel;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Vector3f;
import renderEngine.DisplayManager;
import terrain.TerrainTileSet;

import java.util.Arrays;

public class Player extends Entity {
    private static final float RUN_SPEED = 50;
    private static final float STRAFE_SPEED = 30;
    private static final float TURN_SPEED = 160;
    private static final float JUMP_POWER = 30;

    private float forwardSpeed = 0;
    private float strafeSpeed = 0;
    private float turnSpeed = 0;
    private float upwardsSpeed = 0;

    private boolean isAirborne = false;

    public Player(TexturedModel model, Vector3f position, float rotX, float rotY, float rotZ, float scale) {
        super(model, position, rotX, rotY, rotZ, scale);
    }

    public void move(TerrainTileSet terrainTileSet) {
        processInputs();

        boolean isRunning = Arrays.binarySearch(new float[]{ this.forwardSpeed, -this.forwardSpeed }, RUN_SPEED) >= 0;
        boolean isStrafing = Arrays.binarySearch(new float[]{ this.strafeSpeed, -this.strafeSpeed }, STRAFE_SPEED) >= 0;

        if (isRunning && isStrafing) {
            this.forwardSpeed /= 1.5;
            this.strafeSpeed /= 1.5;
        }

        float forwardDistance = forwardSpeed * DisplayManager.getFrameTimeSeconds();
        float dx = (float) (forwardDistance * Math.sin(Math.toRadians(getRotY())));
        float dz = (float) (forwardDistance * Math.cos(Math.toRadians(getRotY())));

        float GRAVITY = -80;
        upwardsSpeed += GRAVITY * DisplayManager.getFrameTimeSeconds();
        float dy = upwardsSpeed * DisplayManager.getFrameTimeSeconds();

        float strafeDistance = strafeSpeed * DisplayManager.getFrameTimeSeconds();
        float strafeDX = (float) (strafeDistance * Math.cos(Math.toRadians(-getRotY())));
        float strafeDZ = (float) (strafeDistance * Math.sin(Math.toRadians(-getRotY())));

        increasePosition(strafeDX + dx, dy, strafeDZ + dz);
        increaseRotation(0, turnSpeed * DisplayManager.getFrameTimeSeconds(), 0);

        float terrainHeight = terrainTileSet.getHeightAt(getPosition().x, getPosition().z);

        if (getPosition().y < terrainHeight) {
            upwardsSpeed = 0;
            isAirborne = false;
            getPosition().y = terrainHeight;
        }
    }

    private void jump() {
        if (isAirborne) return;
        this.upwardsSpeed = JUMP_POWER;
        isAirborne = true;
    }

    private void processInputs() {
        if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
            this.forwardSpeed = RUN_SPEED;
        } else if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
            this.forwardSpeed = -RUN_SPEED;
        } else {
            this.forwardSpeed = 0;
        }

        if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
            if (Mouse.isButtonDown(1)) {
                this.strafeSpeed = -STRAFE_SPEED;
                this.turnSpeed = 0;
            } else {
                this.turnSpeed = -TURN_SPEED;
                this.strafeSpeed = 0;
            }
        } else if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
            if (Mouse.isButtonDown(1)) {
                this.strafeSpeed = STRAFE_SPEED;
                this.turnSpeed = 0;
            } else {
                this.turnSpeed = TURN_SPEED;
                this.strafeSpeed = 0;
            }
        } else {
            this.turnSpeed = 0;
            this.strafeSpeed = 0;
        }

        if (Mouse.isButtonDown(0) && Mouse.isButtonDown(1)) {
            this.forwardSpeed = RUN_SPEED;
        }

        if (Keyboard.isKeyDown(Keyboard.KEY_SPACE)) {
            jump();
        }
    }
}
