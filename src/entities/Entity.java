package entities;

import models.TexturedModel;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

public class Entity {
    private TexturedModel model;
    private Vector3f position;

    private float rotX, rotY, rotZ;
    private float scale;

    private int textureAtlasIndex = 0;

    public Entity(TexturedModel model, Vector3f position) {
        this(model, position, 0, 0, 0, 1);
    }

    public Entity(TexturedModel model, Vector3f position, float rotX, float rotY, float rotZ, float scale) {
        this.model = model;
        this.position = position;
        this.rotX = rotX;
        this.rotY = rotY;
        this.rotZ = rotZ;
        this.scale = scale;
    }

    public Entity(TexturedModel model, int textureAtlasIndex, Vector3f position, float rotX, float rotY, float rotZ, float scale) {
        this(model, position, rotX, rotY, rotZ, scale);
        this.textureAtlasIndex = textureAtlasIndex;
    }

    public void increasePosition(float dx, float dy, float dz) {
        this.position.x += dx;
        this.position.y += dy;
        this.position.z += dz;
    }

    public void increaseRotation(float dx, float dy, float dz) {
        this.rotX += dx;
        this.rotY += dy;
        this.rotZ += dz;
    }

    public TexturedModel getModel() {
        return model;
    }

    public Vector3f getPosition() {
        return position;
    }

    public Vector2f getTextureOffsets() {
        int atlasSize = model.getTexture().getAtlasSize();
        int column = textureAtlasIndex % atlasSize;
        int row = textureAtlasIndex / atlasSize;
        float x = (float) column / atlasSize;
        float y = (float) row / atlasSize;

        return new Vector2f(x, y);
    }

    public void setPosition(Vector3f position) {
        this.position = position;
    }

    public float getRotX() {
        return rotX;
    }

    public void setRotX(float rotX) {
        this.rotX = rotX;
    }

    public float getRotY() {
        return rotY;
    }

    public void setRotY(float rotY) {
        this.rotY = rotY;
    }

    public float getRotZ() {
        return rotZ;
    }

    public void setRotZ(float rotZ) {
        this.rotZ = rotZ;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }
}
