package entities;

import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Vector3f;
import utils.Maths;

public class Camera {
    private float distanceFromPlayer = 30;
    private float angleAroundPlayer = 0;

    private Vector3f position = new Vector3f(0,5,200);
    private float pitch = 10;
    private float yaw;

    private Player player;

    public Camera(Player player) {
        this.player = player;
    }

    public void move() {
        calculateZoom();
        processInput();
        float horizontalDistance = calculateHorizontalDistance();
        float verticalDistance = calculateVerticalDistance();
        calculatePosition(horizontalDistance, verticalDistance);
        this.yaw = 180 - (player.getRotY() + angleAroundPlayer);
    }

    public Vector3f getPosition() {
        return position;
    }

    public float getPitch() {
        return pitch;
    }

    public float getYaw() {
        return yaw;
    }

    public void invertPitch() {
        this.pitch = -pitch;
    }

    private void calculatePosition(float horizontalDistance, float verticalDistance) {
        float theta = player.getRotY() + angleAroundPlayer;
        float offsetX = (float) (horizontalDistance * Math.sin(Math.toRadians(theta)));
        float offsetZ = (float) (horizontalDistance * Math.cos(Math.toRadians(theta)));
        position.x = player.getPosition().x - offsetX;
        position.z = player.getPosition().z - offsetZ;
        position.y = Maths.clamp(2.0f, 100.0f, player.getPosition().y + verticalDistance + 3);
    }

    private float calculateHorizontalDistance() {
        return (float) (distanceFromPlayer * Math.cos(Math.toRadians(pitch)));
    }

    private float calculateVerticalDistance() {
        return (float) (distanceFromPlayer * Math.sin(Math.toRadians(pitch)));
    }

    private void calculateZoom() {
        float zoomLevel = Mouse.getDWheel() * 0.1f;
        distanceFromPlayer = Maths.clamp(0, 150, distanceFromPlayer - zoomLevel);
    }

    private void processInput() {
        if (Mouse.isButtonDown(1)) {
            calculatePitch();
            float angleChange = Maths.clamp(-9.0f, 9.0f, Mouse.getDX() * 0.1f);
            player.increaseRotation(0, -angleChange, 0);

            if (angleAroundPlayer != 0) {
                if (angleAroundPlayer < 0) {
                    angleAroundPlayer = (float) Math.ceil(angleAroundPlayer + (-angleAroundPlayer / 50));
                    if (angleAroundPlayer > 0) angleAroundPlayer = 0;
                } else if (angleAroundPlayer > 0) {
                    angleAroundPlayer = (float) Math.floor(angleAroundPlayer - (angleAroundPlayer / 50));
                    if (angleAroundPlayer < 0) angleAroundPlayer = 0;
                }
            }

            Mouse.setGrabbed(true);
        } else if (Mouse.isButtonDown(0)) {
            calculatePitch();
            calculateAngle();
            Mouse.setGrabbed(true);
        } else {
            Mouse.setGrabbed(false);
        }
    }

    private void calculatePitch() {
        float pitchChange = Maths.clamp(-6.0f, 6.0f, Mouse.getDY() * 0.1f);
        pitch -= pitchChange;
    }

    private void calculateAngle() {
        float angleChange = Maths.clamp(-6.0f, 6.0f, Mouse.getDX() * 0.3f);
        angleAroundPlayer -= angleChange;
    }
}
