package engineTester;

import entities.Entity;
import models.RawModel;
import models.TexturedModel;
import objConversion.ModelData;
import objConversion.OBJFileLoader;
import org.lwjgl.util.vector.Vector3f;
import renderEngine.Loader;
import terrain.TerrainTile;
import terrain.TerrainTileSet;
import textures.ModelTexture;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FoliageEntityGenerator {
    private Loader loader;

    public FoliageEntityGenerator(Loader loader) {
        this.loader = loader;
    }

    public List<Entity> generateForTerrain(TerrainTileSet terrainTileSet) {
        Random random = new Random();

        ModelData fernModelData = OBJFileLoader.loadOBJ("fern");
        RawModel fernModel = loader.initialiseVAO(fernModelData);
        ModelTexture fernTextureAtlas = new ModelTexture(loader.loadTexture("fern"));
        fernTextureAtlas.setAtlasSize(2);
        TexturedModel fernTexturedModel = new TexturedModel(fernModel, fernTextureAtlas);
        fernTexturedModel.getTexture().setShineDamper(10);
        fernTexturedModel.getTexture().setReflectivity(0.05f);
        fernTexturedModel.getTexture().setHasTransparency(true);

        List<Entity> fernEntityList = new ArrayList<>();
        for (int i = 0; i < 500; i++) {
            float x = random.nextFloat() * TerrainTile.SIZE;
            float z = random.nextFloat() * TerrainTile.SIZE;
            float y = terrainTileSet.getHeightAt(x, z) - 1;

            fernEntityList.add(new Entity(fernTexturedModel, random.nextInt(4), new Vector3f(x, y, z),
                    0,1.0f + random.nextFloat() * (3.5f - 1.0f) * 100,0,
                    1.0f + random.nextFloat() * (1.5f - 1.0f)));
        }

        return fernEntityList;
    }
}
