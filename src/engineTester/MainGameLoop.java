package engineTester;

import entities.Camera;
import entities.Entity;
import entities.Light;
import entities.Player;
import gui.GuiRenderer;
import gui.GuiElement;
import gui.text.*;
import models.RawModel;
import models.TexturedModel;
import objConversion.ModelData;
import objConversion.OBJFileLoader;
import objConversion.normalMapping.NormalMappedObjLoader;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import particles.Particle;
import particles.ParticleManager;
import particles.ParticleSystem;
import particles.ParticleTexture;
import postProcessing.Fbo;
import postProcessing.PostProcessing;
import renderEngine.*;
import terrain.TerrainTileSet;
import textures.ModelTexture;
import utils.MousePicker;
import water.WaterFrameBuffers;
import water.WaterRenderer;
import water.WaterShader;
import water.WaterTile;

import java.io.File;
import java.util.*;

public class MainGameLoop {
    public static void main(String[] args) {
        DisplayManager.createDisplay();
        Loader loader = new Loader();

        // Lamps
        ModelData lampModelData = OBJFileLoader.loadOBJ("lamp");
        RawModel lampModel = loader.initialiseVAO(lampModelData);
        TexturedModel lampTexturedModel = new TexturedModel(lampModel, new ModelTexture(loader.loadTexture("lamp")));
        lampTexturedModel.getTexture().setUsesFakeLighting(true);

        List<Entity> entities = new ArrayList<>();
        List<Entity> normalMapEntities = new ArrayList<>();

        TerrainTileSet terrain = loadTerrain(loader);
        entities.addAll(loadTrees(loader, terrain));
        entities.addAll(loadFoliage(loader, terrain));

        List<Entity> lampEntityList = new ArrayList<>();
        lampEntityList.add(new Entity(lampTexturedModel, new Vector3f(80, terrain.getHeightAt(80, 120), 120)));
        lampEntityList.add(new Entity(lampTexturedModel, new Vector3f(250, terrain.getHeightAt(250, 250), 250)));
        lampEntityList.add(new Entity(lampTexturedModel, new Vector3f(380, terrain.getHeightAt(380, 420), 420)));
        entities.addAll(lampEntityList);

        List<Light> lights = new ArrayList<>();
        lights.add(new Light(new Vector3f(400,2000,400), new Vector3f(1.0f, 1.0f, 1.8f)));
        lights.add(new Light(new Vector3f(80,terrain.getHeightAt(80, 120) + 20,120), new Vector3f(2.0f, 1.0f, 4.0f), new Vector3f(1, 0.01f, 0.002f)));
        lights.add(new Light(new Vector3f(250,terrain.getHeightAt(250, 250) + 20,250), new Vector3f(0.9f, 0.2f, 1.7f), new Vector3f(1, 0.01f, 0.002f)));
        lights.add(new Light(new Vector3f(380,terrain.getHeightAt(380, 420) + 20,420), new Vector3f(0.8f, 3.1f, 1.2f), new Vector3f(1, 0.01f, 0.002f)));
        lights.add(new Light(new Vector3f(0, 0, 0), new Vector3f(2.5f,1.5f,1.0f), new Vector3f(1, 0.01f, 0.002f)));

        TexturedModel barrelModel = new TexturedModel(NormalMappedObjLoader.loadOBJ("barrel", loader), new ModelTexture(loader.loadTexture("barrel")));
        barrelModel.getTexture().setNormalMap(loader.loadTexture("barrelNormal").getTextureID());
        barrelModel.getTexture().setShineDamper(20);
        barrelModel.getTexture().setReflectivity(0.2f);
        normalMapEntities.add(new Entity(barrelModel, new Vector3f(400, terrain.getHeightAt(400, 100) + 2, 100), 0, 0, 0, 0.6f));

        Player player = loadPlayer(loader);
        player.setPosition(new Vector3f(200, terrain.getHeightAt(200, 200), 200));
        entities.add(player);

        Camera camera = new Camera(player);

        MasterRenderer renderer = new MasterRenderer(loader, camera);
        ParticleManager.init(loader, renderer.getProjectionMatrix());

        final float WATER_HEIGHT = -3;
        WaterFrameBuffers frameBuffers = new WaterFrameBuffers();
        WaterShader waterShader = new WaterShader();
        WaterRenderer waterRenderer = new WaterRenderer(loader, waterShader, renderer.getProjectionMatrix(), frameBuffers);
        List<WaterTile> waterTiles = new ArrayList<>();
        waterTiles.add(new WaterTile(100, 100, WATER_HEIGHT));
        waterTiles.add(new WaterTile(100, 300, WATER_HEIGHT));
        waterTiles.add(new WaterTile(100, 500, WATER_HEIGHT));
        waterTiles.add(new WaterTile(100, 700, WATER_HEIGHT));
        waterTiles.add(new WaterTile(300, 100, WATER_HEIGHT));
        waterTiles.add(new WaterTile(300, 300, WATER_HEIGHT));
        waterTiles.add(new WaterTile(300, 500, WATER_HEIGHT));
        waterTiles.add(new WaterTile(300, 700, WATER_HEIGHT));
        waterTiles.add(new WaterTile(500, 100, WATER_HEIGHT));
        waterTiles.add(new WaterTile(500, 300, WATER_HEIGHT));
        waterTiles.add(new WaterTile(500, 500, WATER_HEIGHT));
        waterTiles.add(new WaterTile(500, 700, WATER_HEIGHT));
        waterTiles.add(new WaterTile(700, 100, WATER_HEIGHT));
        waterTiles.add(new WaterTile(700, 300, WATER_HEIGHT));
        waterTiles.add(new WaterTile(700, 500, WATER_HEIGHT));
        waterTiles.add(new WaterTile(700, 700, WATER_HEIGHT));

        List<GuiElement> guiElements = new ArrayList<>();
        guiElements.add(new GuiElement(loader.loadTexture("gui/hud/bar_energy", false), new Vector2f(0f, -0.9f), 860, 80));
        guiElements.add(new GuiElement(frameBuffers.getReflectionTexture(), 1.0f, 1.0f, new Vector2f(-0.7f, 0.7f), 480, 270));
        guiElements.add(new GuiElement(frameBuffers.getRefractionTexture(), 1.0f, -1.0f, new Vector2f(0.7f, 0.7f), 480, 270));
        guiElements.add(new GuiElement(renderer.getShadowMapTexture(), 1.0f, 1.0f, new Vector2f(-0.7f, -0.7f), 480, 270));

        GuiRenderer guiRenderer = new GuiRenderer(loader);

        TextRenderer textRenderer = new TextRenderer(loader);
        Font regularFont = new Font(loader.loadTexture("gui/fonts/noto-serif", false), new File("res/textures/gui/fonts/noto-serif.json"));
        Font boldFont = new Font(loader.loadTexture("gui/fonts/noto-serif-bold", false), new File("res/textures/gui/fonts/noto-serif-bold.json"));
        FontSet notoSerif = new FontSet(regularFont, boldFont);
        TextGroup textGroup = new TextGroup(800, notoSerif, 0.5, TextGroup.Alignment.CENTER);

//        MousePicker picker = new MousePicker(camera, renderer.getProjectionMatrix(), terrain);

//        ParticleTexture particleTexture = new ParticleTexture(loader.loadTexture("particles/EXPOROSHUN").getTextureID(), 8, true);
//        ParticleSystem particleSystem = new ParticleSystem(particleTexture, 40, 10, 0.1f, 1, 1.6f);

        Fbo fbo = new Fbo(Display.getWidth(), Display.getHeight(), Fbo.DEPTH_RENDER_BUFFER);
        PostProcessing.init(loader);

        while (!Display.isCloseRequested()) {
            camera.move();
            player.move(terrain);

//            particleSystem.generateParticles(player.getPosition());
//            ParticleManager.update(camera);

            GL11.glEnable(GL30.GL_CLIP_DISTANCE0);

//            picker.update();
//            Vector3f terrainPoint = picker.getCurrentTerrainPoint();
//            if (terrainPoint != null) {
//                lights.get(4).setPosition(new Vector3f(terrainPoint.x, terrainPoint.y + 15, terrainPoint.z));
//            }

            if (Keyboard.isKeyDown(Keyboard.KEY_G)) {
                GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
            } else {
                GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_FILL);
            }

            // Reflection
            frameBuffers.bindReflectionFrameBuffer();
            float distance = 2 * (camera.getPosition().y - WATER_HEIGHT);
            camera.getPosition().y -= distance;
            camera.invertPitch();
            renderer.renderScene(entities, normalMapEntities, terrain, lights, camera, new Vector4f(0, 1, 0, -WATER_HEIGHT + 1.0f));
            ParticleManager.renderParticles(camera);
            frameBuffers.unbindCurrentFrameBuffer();
            camera.getPosition().y += distance;
            camera.invertPitch();

            // Refraction
            frameBuffers.bindRefractionFrameBuffer();
            renderer.renderScene(entities, normalMapEntities, terrain, lights, camera, new Vector4f(0, -1, 0, WATER_HEIGHT + 1.0f));

            // Render to screen
            GL11.glDisable(GL30.GL_CLIP_DISTANCE0);
            frameBuffers.unbindCurrentFrameBuffer();

            renderer.renderShadowMap(entities, lights.get(0));

            fbo.bindFrameBuffer();
            renderer.renderScene(entities, normalMapEntities, terrain, lights, camera, new Vector4f(0, -1, 0, 10000));
            waterRenderer.render(waterTiles, camera, lights.get(0));

//            ParticleManager.renderParticles(camera);

            fbo.unbindFrameBuffer();
            PostProcessing.doPostProcessing(fbo.getColourTexture());

            if (!Keyboard.isKeyDown(Keyboard.KEY_H)) {
                guiRenderer.render(guiElements);
            }
            textRenderer.render(textGroup);

            DisplayManager.updateDisplay();
        }

        PostProcessing.cleanUp();
        fbo.cleanUp();
        renderer.cleanUp();
        frameBuffers.cleanUp();
        waterShader.cleanUp();
        guiRenderer.cleanUp();
        textRenderer.cleanUp();
        loader.cleanUp();
        ParticleManager.cleanUp();
        DisplayManager.closeDisplay();
    }

    private static TerrainTileSet loadTerrain(Loader loader) {
        TerrainGenerator terrainGenerator = new TerrainGenerator(loader);
        return terrainGenerator.generate(4, 4);
    }

    private static List<Entity> loadTrees(Loader loader, TerrainTileSet terrainTileSet) {
        TreeEntityGenerator treeEntityGenerator = new TreeEntityGenerator(loader);
        return treeEntityGenerator.generateForTerrain(terrainTileSet);
    }

    private static List<Entity> loadFoliage(Loader loader, TerrainTileSet terrainTileSet) {
        FoliageEntityGenerator foliageEntityGenerator = new FoliageEntityGenerator(loader);
        return foliageEntityGenerator.generateForTerrain(terrainTileSet);
    }

    private static Player loadPlayer(Loader loader) {
        PlayerCharacterGenerator playerCharacterGenerator = new PlayerCharacterGenerator(loader);
        return playerCharacterGenerator.generate();
    }
}
