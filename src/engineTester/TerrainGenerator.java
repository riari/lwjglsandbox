package engineTester;

import renderEngine.Loader;
import terrain.TerrainTile;
import terrain.TerrainTileSet;
import textures.TerrainTexture;
import textures.TerrainTexturePack;

public class TerrainGenerator {
    private Loader loader;

    public TerrainGenerator(Loader loader) {
        this.loader = loader;
    }

    public TerrainTileSet generate(int totalSizeX, int totalSizeZ) {
        TerrainTexturePack texturePack = loadTextures();
        TerrainTexture blendMap = loadBlendMap();

        TerrainTileSet terrainTileSet = new TerrainTileSet();
        for (int x = 0; x < totalSizeX; x++) {
            for (int z = 0; z < totalSizeZ; z++) {
                TerrainTile tile = new TerrainTile(x, z, loader, texturePack, blendMap);
                terrainTileSet.add(tile);
            }
        }

        return terrainTileSet;
    }

    private TerrainTexturePack loadTextures() {
        TerrainTexture backgroundTexture = new TerrainTexture(loader.loadTexture("grass"));
        TerrainTexture rTexture = new TerrainTexture(loader.loadTexture("soil1"));
        TerrainTexture gTexture = new TerrainTexture(loader.loadTexture("soil2"));
        TerrainTexture bTexture = new TerrainTexture(loader.loadTexture("stonePath"));

        return new TerrainTexturePack(backgroundTexture, rTexture, gTexture, bTexture);
    }

    private TerrainTexture loadBlendMap() {
        return new TerrainTexture(loader.loadTexture("blendMap"));
    }
}
