package engineTester;

import entities.Player;
import models.RawModel;
import models.TexturedModel;
import objConversion.ModelData;
import objConversion.OBJFileLoader;
import org.lwjgl.util.vector.Vector3f;
import renderEngine.Loader;
import textures.ModelTexture;

public class PlayerCharacterGenerator {
    private Loader loader;

    public PlayerCharacterGenerator(Loader loader) {
        this.loader = loader;
    }

    public Player generate() {
        ModelData playerData = OBJFileLoader.loadOBJ("catto");
        RawModel playerModel = loader.initialiseVAO(playerData);
        TexturedModel playerTexturedModel = new TexturedModel(playerModel, new ModelTexture(loader.loadTexture("white")));
        return new Player(playerTexturedModel, new Vector3f(0,0,150), 0 ,0, 0, 0.35f);
    }
}
