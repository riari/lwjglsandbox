package engineTester;

import entities.Entity;
import models.RawModel;
import models.TexturedModel;
import objConversion.ModelData;
import objConversion.OBJFileLoader;
import org.lwjgl.util.vector.Vector3f;
import renderEngine.Loader;
import terrain.TerrainTile;
import terrain.TerrainTileSet;
import textures.ModelTexture;
import utils.Maths;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TreeEntityGenerator {
    private Loader loader;

    public TreeEntityGenerator(Loader loader) {
        this.loader = loader;
    }

    public List<Entity> generateForTerrain(TerrainTileSet terrainTileSet) {
        Random random = new Random();

        ModelData treeData = OBJFileLoader.loadOBJ("tree");
        RawModel treeModel = loader.initialiseVAO(treeData);
        List<TexturedModel> treeTexturedModels = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            TexturedModel treeTexturedModel = new TexturedModel(treeModel, new ModelTexture(loader.loadTexture("tree")));
            treeTexturedModel.getTexture().setShineDamper(10);
            treeTexturedModel.getTexture().setReflectivity(0.05f);
            treeTexturedModel.getTexture().setTint(new Vector3f(
                    Maths.clamp(-0.1f, 0.08f, random.nextFloat() * 0.16f - 0.08f),
                    Maths.clamp(-0.1f, 0.04f, random.nextFloat() * 0.8f - 0.04f), 0));
            treeTexturedModels.add(treeTexturedModel);
        }

        List<Entity> treeEntityList = new ArrayList<>();
        for (int i = 0; i < 500; i++) {
            float x = random.nextFloat() * TerrainTile.SIZE;
            float z = random.nextFloat() * TerrainTile.SIZE;
            float y = terrainTileSet.getHeightAt(x, z) - 2;

            treeEntityList.add(new Entity(treeTexturedModels.get(random.nextInt(5)), new Vector3f(x, y, z),
                    0,1.0f + random.nextFloat() * (3.5f - 1.0f) * 100,0,
                    1.0f + random.nextFloat() * (2.5f - 1.0f)));
        }

        return treeEntityList;
    }
}
