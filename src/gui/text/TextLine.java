package gui.text;

import java.util.ArrayList;
import java.util.List;

public class TextLine {
    private double spaceSize;
    private double maxWidth;
    private double width = 0;
    private List<TextWord> words = new ArrayList<>();

    public TextLine(double spaceWidth, double fontSize, double maxWidth) {
        this.spaceSize = spaceWidth * fontSize;
        this.maxWidth = maxWidth;
    }

    protected boolean tryAddWord(TextWord word) {
        double wordWidth = word.getWidth();
        wordWidth += !words.isEmpty() ? spaceSize : 0;

        if (width + wordWidth > maxWidth) {
            return false;
        }

        words.add(word);
        width += wordWidth;

        return true;
    }

    protected double getMaxWidth() {
        return maxWidth;
    }

    protected double getWidth() {
        return width;
    }

    protected List<TextWord> getWords() {
        return words;
    }
}
