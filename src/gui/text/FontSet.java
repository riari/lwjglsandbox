package gui.text;

public class FontSet {
    private Font regular;
    private Font bold;
    private Font italic;

    public FontSet(Font regular) {
        this(regular, null, null);
    }

    public FontSet(Font regular, Font bold) {
        this(regular, bold, null);
    }

    public FontSet(Font regular, Font bold, Font italic) {
        this.regular = regular;
        this.bold = bold;
        this.italic = italic;
    }

    public Font getRegular() {
        return regular;
    }

    public boolean hasBold() {
        return bold != null;
    }

    public Font getBold() {
        return bold;
    }

    public boolean hasItalic() {
        return italic != null;
    }

    public Font getItalic() {
        return italic;
    }
}
