package gui.text;

public class TextMeshData {
    private Font font;
    private float[] vertexPositions;
    private float[] textureCoords;

    TextMeshData(Font font, float[] vertexPositions, float[] textureCoords) {
        this.font = font;
        this.vertexPositions = vertexPositions;
        this.textureCoords = textureCoords;
    }

    public Font getFont() {
        return font;
    }

    public float[] getVertexPositions() {
        return vertexPositions;
    }

    public float[] getTextureCoords() {
        return textureCoords;
    }

    public int getVertexCount() {
        return vertexPositions.length / 2;
    }
}
