package gui.text;

import java.util.ArrayList;
import java.util.List;

public class TextWord {
    private List<TextCharacter> characters = new ArrayList<>();
    private double fontScale;
    private double width = 0;

    TextWord(double fontScale) {
        this.fontScale = fontScale;
    }

    void addCharacter(FontSet fonts, char c, boolean isBold, boolean isItalic) {
        Font font = fonts.getRegular();
        if (isBold && fonts.hasBold()) {
            font = fonts.getBold();
        } else if (isItalic && fonts.hasItalic()) {
            font = fonts.getItalic();
        }

        characters.add(new TextCharacter(c, isBold, isItalic));
        width += font.getGlyph(c).getAdvance() * fontScale;
    }

    List<TextCharacter> getCharacters() {
        return characters;
    }

    protected double getWidth() {
        return width;
    }
}
