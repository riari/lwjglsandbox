package gui.text;

import org.lwjgl.opengl.*;
import org.lwjgl.util.vector.Vector3f;
import renderEngine.Loader;
import utils.Data;

import java.util.ArrayList;
import java.util.List;

public class TextRenderer {
    private enum Format {
        REGULAR, BOLD, ITALIC
    }

    private Loader loader;
    private TextShader shader;

    public TextRenderer(Loader loader) {
        this.loader = loader;
        this.shader = new TextShader();
    }

    public void render(TextGroup textGroup) {
        startRendering();
        shader.loadColour(new Vector3f(1, 1, 1));

        for (TextMeshData meshData : createMeshData(textGroup)) {
            if (meshData.getVertexPositions().length == 0) continue;

            int vaoID = loader.initialiseVAO(meshData.getVertexPositions(), meshData.getTextureCoords());

            bindFontTexture(meshData.getFont().getTexture().getTextureID());

            GL30.glBindVertexArray(vaoID);
            GL20.glEnableVertexAttribArray(0);
            GL20.glEnableVertexAttribArray(1);
            GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, meshData.getVertexCount());
            GL20.glDisableVertexAttribArray(0);
            GL20.glDisableVertexAttribArray(1);
            GL30.glBindVertexArray(0);
        }

        stopRendering();
    }

    private void bindFontTexture(int textureID) {
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureID);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);
    }

    private TextMeshData[] createMeshData(TextGroup textGroup) {
        double fontScale = textGroup.getFontScale();
        Font font = textGroup.getFonts().getRegular();
        double cursorX;
        double cursorY = font.getData().getLineHeight();

        List<Float> regularVertices = new ArrayList<>();
        List<Float> regularTextureCoords = new ArrayList<>();
        List<Float> boldVertices = new ArrayList<>();
        List<Float> boldTextureCoords = new ArrayList<>();
        List<Float> italicVertices = new ArrayList<>();
        List<Float> italicTextureCoords = new ArrayList<>();
        Format currentFormat = Format.REGULAR;
        for (TextLine line : textGroup.getLines()) {
            switch (textGroup.getAlignment()) {
                case CENTER:
                    cursorX = (Display.getWidth() - line.getWidth()) / 2;
                    break;
                case RIGHT:
                    cursorX = Display.getWidth() - line.getWidth();
                    break;
                default:
                    cursorX = 0;
                    break;
            }

            for (TextWord word : line.getWords()) {
                for (TextCharacter character : word.getCharacters()) {
                    if (character.isBold() && currentFormat != Format.BOLD && textGroup.getFonts().hasBold()) {
                        currentFormat = Format.BOLD;
                        font = textGroup.getFonts().getBold();
                    } else if (character.isItalic() && currentFormat != Format.ITALIC && textGroup.getFonts().hasItalic()) {
                        currentFormat = Format.ITALIC;
                        font = textGroup.getFonts().getItalic();
                    } else if (!character.isBold() && !character.isItalic() && currentFormat != Format.REGULAR) {
                        currentFormat = Format.REGULAR;
                        font = textGroup.getFonts().getRegular();
                    }

                    FontGlyph fontGlyph = font.getGlyph(character.getChar());

                    double[] xy = createVerticesForCharacter(fontGlyph, cursorX, cursorY, fontScale);
                    double[] st = createTextureCoordsForCharacter(fontGlyph, font.getTexture().getTextureWidth(), font.getTexture().getTextureHeight());

                    if (!character.isBold() && !character.isItalic()) {
                        addPositions(regularVertices, xy);
                        addPositions(regularTextureCoords, st);
                    } else if (character.isBold()) {
                        addPositions(boldVertices, xy);
                        addPositions(boldTextureCoords, st);
                    } else {
                        addPositions(italicVertices, xy);
                        addPositions(italicTextureCoords, st);
                    }

                    cursorX += fontGlyph.getAdvance() * fontScale;
                }

                cursorX += font.getSpaceWidth() * fontScale;
                font = textGroup.getFonts().getRegular();
            }

            cursorY += font.getData().getLineHeight() * fontScale;
        }

        return new TextMeshData[] {
                new TextMeshData(textGroup.getFonts().getRegular(), Data.floatListToArray(regularVertices), Data.floatListToArray(regularTextureCoords)),
                new TextMeshData(textGroup.getFonts().getBold(), Data.floatListToArray(boldVertices), Data.floatListToArray(boldTextureCoords)),
                new TextMeshData(textGroup.getFonts().getItalic(), Data.floatListToArray(italicVertices), Data.floatListToArray(italicTextureCoords))
        };
    }

    private void addPositions(List<Float> positions, double[] coords) {
        positions.add((float) coords[0]);
        positions.add((float) coords[1]);
        positions.add((float) coords[0]);
        positions.add((float) coords[3]);
        positions.add((float) coords[2]);
        positions.add((float) coords[3]);
        positions.add((float) coords[2]);
        positions.add((float) coords[3]);
        positions.add((float) coords[2]);
        positions.add((float) coords[1]);
        positions.add((float) coords[0]);
        positions.add((float) coords[1]);
    }

    private double[] createVerticesForCharacter(FontGlyph glyph, double cursorX, double cursorY, double fontScale) {
        final double xScale = 1.0 / Display.getWidth();
        final double yScale = 1.0 / Display.getHeight();

        double x0 = (cursorX - (glyph.getOriginX() * fontScale)) * xScale;
        double y0 = (cursorY - (glyph.getOriginY() * fontScale)) * yScale;
        double x1 = x0 + (glyph.getWidth() * fontScale * xScale);
        double y1 = y0 + (glyph.getHeight() * fontScale * yScale);

        return new double[] { (2 * x0) - 1, (-2 * y0) + 1, (2 * x1) - 1, (-2 * y1) + 1 };
    }

    private double[] createTextureCoordsForCharacter(FontGlyph glyph, double textureWidth, double textureHeight) {
        final double xScale = 1.0 / textureWidth;
        final double yScale = 1.0 / textureHeight;

        double s0 = glyph.getX() * xScale;
        double t0 = glyph.getY() * yScale;
        double s1 = s0 + (glyph.getWidth() * xScale);
        double t1 = t0 + (glyph.getHeight() * yScale);

        return new double[] { s0, t0, s1, t1 };
    }

    public void cleanUp() {
        shader.cleanUp();
    }

    private void startRendering() {
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glDisable(GL11.GL_DEPTH_TEST);
        shader.start();
    }

    private void stopRendering() {
        shader.stop();
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glEnable(GL11.GL_DEPTH_TEST);
    }
}
