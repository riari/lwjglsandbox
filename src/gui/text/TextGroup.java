package gui.text;

import utils.Tokenizer;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class TextGroup {
    static final int TOKEN_CHARACTER = 0;
    static final int TOKEN_BOLD_BEGIN = 1;
    static final int TOKEN_BOLD_END = 2;
    static final int TOKEN_ITALIC_BEGIN = 3;
    static final int TOKEN_ITALIC_END = 4;
    static final int TOKEN_WHITESPACE = 5;

    public enum Alignment {
        LEFT, CENTER, RIGHT
    }

    private int width;
    private FontSet fonts;
    private double fontScale;
    private Alignment alignment;

    private List<TextLine> lines = new ArrayList<>();

    public TextGroup(int width, FontSet fonts, double fontScale) {
        this(width, fonts, fontScale, TextGroup.Alignment.LEFT);
    }

    public TextGroup(int width, FontSet fonts, double fontScale, Alignment alignment) {
        this.width = width;
        this.fonts = fonts;
        this.fontScale = fontScale;
        this.alignment = alignment;
    }

    public void add(String text) {
        Font font = fonts.getRegular();
        TextLine currentLine = new TextLine(font.getSpaceWidth(), fontScale, width);
        TextWord currentWord = new TextWord(fontScale);

        Tokenizer tokenizer = new Tokenizer();
        tokenizer.addToken("\\[(b|B)]", TOKEN_BOLD_BEGIN);
        tokenizer.addToken("\\[/(b|B)]", TOKEN_BOLD_END);
        tokenizer.addToken("\\[(i|I)]", TOKEN_ITALIC_BEGIN);
        tokenizer.addToken("\\[/(i|I)]", TOKEN_ITALIC_END);
        tokenizer.addToken("\\s+", TOKEN_WHITESPACE);
        tokenizer.addToken(".", TOKEN_CHARACTER);

        ArrayList<Tokenizer.Token> tokens = tokenizer.tokenize(text);

        System.out.println("Generated tokens:");
        System.out.println(tokens);

        boolean bold = false;
        boolean italic = false;
        Stack<Integer> state = new Stack<>();

        parseLoop:
        for (Tokenizer.Token token : tokens) {
            switch (token.getKey()) {
                case TOKEN_BOLD_BEGIN:
                    state.push(TOKEN_BOLD_BEGIN);
                    bold = true;
                    break;
                case TOKEN_BOLD_END:
                    if (state.peek() != TOKEN_BOLD_BEGIN) {
                        break parseLoop;    // XXX: Use `continue;` instead to ignore error
                    }
                    bold = false;
                    state.pop();
                    break;
                case TOKEN_ITALIC_BEGIN:
                    state.push(TOKEN_ITALIC_BEGIN);
                    italic = true;
                    break;
                case TOKEN_ITALIC_END:
                    if (state.peek() != TOKEN_ITALIC_BEGIN) {
                        break parseLoop;    // XXX: Use `continue;` instead to ignore error
                    }
                    italic = false;
                    state.pop();
                    break;
                case TOKEN_WHITESPACE:
                    if (!currentLine.tryAddWord(currentWord)) {
                        lines.add(currentLine);
                        currentLine = new TextLine(font.getSpaceWidth(), fontScale, width);
                        currentLine.tryAddWord(currentWord);
                    }
                    currentWord = new TextWord(fontScale);
                    break;
                case TOKEN_CHARACTER:
                    currentWord.addCharacter(fonts, token.getValue().charAt(0), bold, italic);
                    break;
                default:
                    // TODO: Error handling?
                    break;
            }
        }

        if (!currentLine.tryAddWord(currentWord)) {
            lines.add(currentLine);
            currentLine = new TextLine(font.getSpaceWidth(), fontScale, width);
            currentLine.tryAddWord(currentWord);
        }

        lines.add(currentLine);
    }

    public FontSet getFonts() {
        return fonts;
    }

    public double getFontScale() {
        return fontScale;
    }

    public Alignment getAlignment() {
        return alignment;
    }

    public List<TextLine> getLines() {
        return lines;
    }
}
