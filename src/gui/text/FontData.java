package gui.text;

import java.util.Map;

public class FontData {
    private String name;
    private int size;
    private boolean bold;
    private boolean italic;
    private int width;
    private int height;
    private int lineHeight;
    private Map<Character, FontGlyph> glyphs;

    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }

    public boolean isBold() {
        return bold;
    }

    public boolean isItalic() {
        return italic;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getLineHeight() {
        return lineHeight;
    }

    public Map<Character, FontGlyph> getGlyphs() {
        return glyphs;
    }
}
