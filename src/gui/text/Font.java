package gui.text;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.stream.JsonReader;
import org.newdawn.slick.opengl.Texture;

public class Font {
	private Texture texture;
	private FontData data;
	private double spaceWidth;

	public Font(Texture texture, File dataFile) {
		this.texture = texture;

		try (JsonReader reader = new JsonReader(new FileReader(dataFile))) {
			Gson gson = new Gson();
			data = gson.fromJson(reader, FontData.class);
			spaceWidth = getGlyph(' ').getAdvance();
		} catch (JsonParseException | IOException e) {
			e.printStackTrace();
			System.err.println("Failed to read or parse JSON font file!");
		}
	}

	public Texture getTexture() {
		return texture;
	}

	FontData getData() {
		return data;
	}

	double getSpaceWidth() {
		return spaceWidth;
	}

	FontGlyph getGlyph(char ascii) {
		return data.getGlyphs().get(ascii);
	}
}
