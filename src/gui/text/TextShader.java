package gui.text;

import org.lwjgl.util.vector.Vector3f;
import shaders.ShaderProgram;

public class TextShader extends ShaderProgram {
    private static final String VERTEX_FILE = "TextVS";
    private static final String FRAGMENT_FILE = "TextFS";

    private int location_colour;
//    private int location_translation;

    public TextShader() {
        super(VERTEX_FILE, FRAGMENT_FILE);
    }

    @Override
    protected void getAllUniformLocations() {
        location_colour = getUniformLocation("colour");
//        location_translation = getUniformLocation("translation");
    }

    @Override
    protected void bindAttributes() {
        bindAttribute(0, "position");
        bindAttribute(1, "textureCoords");
    }

    protected void loadColour(Vector3f colour) {
        load3DVector(location_colour, colour);
    }

//    protected void loadTranslation(Vector2f translation) {
//        load2DVector(location_translation, translation);
//    }
}
