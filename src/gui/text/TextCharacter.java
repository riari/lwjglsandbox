package gui.text;

public class TextCharacter {
    private char c;
    private boolean isBold;
    private boolean isItalic;

    TextCharacter(char c, boolean isBold, boolean isItalic) {
        this.c = c;
        this.isBold = isBold;
        this.isItalic = isItalic;
    }

    public char getChar() {
        return c;
    }

    public boolean isBold() {
        return isBold;
    }

    public boolean isItalic() {
        return isItalic;
    }
}
