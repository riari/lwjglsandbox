package gui;

import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector2f;
import org.newdawn.slick.opengl.Texture;

public class GuiElement {
    private int texture;
    private float textureOffsetX;
    private float textureOffsetY;
    private Vector2f position;
    private int sizeX;
    private int sizeY;

    public GuiElement(Texture texture, Vector2f position, int sizeX, int sizeY) {
        this(texture.getTextureID(), texture.getWidth(), texture.getHeight(), position, sizeX, sizeY);
    }

    public GuiElement(int texture, float textureOffsetX, float textureOffsetY, Vector2f position, int sizeX, int sizeY) {
        this.texture = texture;
        this.textureOffsetX = textureOffsetX;
        this.textureOffsetY = textureOffsetY;
        this.position = position;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
    }

    public int getTexture() {
        return texture;
    }

    public Vector2f getPosition() {
        return position;
    }

    public Vector2f getTextureOffsets() {
        return new Vector2f(textureOffsetX, textureOffsetY);
    }

    public Vector2f getScale() {
        int displayWidth = Display.getWidth();
        int displayHeight = Display.getHeight();
        float scaleX = (this.sizeX * (1 / (float) displayWidth));
        float scaleY = (this.sizeY * (1 / (float) displayHeight));
        return new Vector2f(scaleX, scaleY);
    }
}
