package gui;

import org.lwjgl.util.vector.Matrix4f;

import org.lwjgl.util.vector.Vector2f;
import shaders.ShaderProgram;

public class GuiShader extends ShaderProgram {
    private static final String VERTEX_FILE = "GuiVS";
    private static final String FRAGMENT_FILE = "GuiFS";

    private int location_transformationMatrix;
    private int location_textureOffsets;

    public GuiShader() {
        super(VERTEX_FILE, FRAGMENT_FILE);
    }

    public void loadTransformation(Matrix4f matrix) {
        loadMatrix(location_transformationMatrix, matrix);
    }

    public void loadTextureOffsets(Vector2f offsets) {
        load2DVector(location_textureOffsets, offsets);
    }

    @Override
    protected void getAllUniformLocations() {
        location_transformationMatrix = getUniformLocation("transformationMatrix");
        location_textureOffsets = getUniformLocation("textureOffsets");
    }

    @Override
    protected void bindAttributes() {
        bindAttribute(0, "position");
    }
}
