package shaders;

import entities.Camera;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import utils.Maths;

public class StaticShader extends ShaderProgram {
    private static final String VERTEX_SRC_FILE = "StaticVS";
    private static final String FRAGMENT_SRC_FILE = "StaticFS";

    private int location_transformationMatrix;

    private int location_projectionMatrix;

    private int location_viewMatrix;

    private int location_skyColour;

    private int location_tintColour;

    private int location_shineDamper;
    private int location_reflectivity;

    private int location_usesFakeLighting;

    private int location_textureAtlasSize;
    private int location_textureOffsets;

    private int location_clippingPlane;

    public StaticShader() {
        super(VERTEX_SRC_FILE, FRAGMENT_SRC_FILE);
    }

    @Override
    protected void bindAttributes() {
        bindAttribute(0, "position");
        bindAttribute(1, "textureCoords");
        bindAttribute(2, "normal");
    }

    @Override
    protected void getAllUniformLocations() {
        location_transformationMatrix = getUniformLocation("transformationMatrix");

        location_projectionMatrix = getUniformLocation("projectionMatrix");

        location_viewMatrix = getUniformLocation("viewMatrix");

        location_lightPosition = new int[MAX_LIGHTS];
        location_lightColour = new int[MAX_LIGHTS];
        location_lightAttenuation = new int[MAX_LIGHTS];
        for (int i = 0; i < MAX_LIGHTS; i++) {
            location_lightPosition[i] = getUniformLocation("lightPosition[" + i + "]");
            location_lightColour[i] = getUniformLocation("lightColour[" + i + "]");
            location_lightAttenuation[i] = getUniformLocation("lightAttenuation[" + i + "]");
        }

        location_skyColour = getUniformLocation("skyColour");

        location_shineDamper = getUniformLocation("shineDamper");
        location_reflectivity = getUniformLocation("reflectivity");

        location_tintColour = getUniformLocation("tintColour");

        location_usesFakeLighting = getUniformLocation("usesFakeLighting");

        location_textureAtlasSize = getUniformLocation("textureAtlasSize");
        location_textureOffsets = getUniformLocation("textureOffsets");

        location_clippingPlane = getUniformLocation("clippingPlane");
    }

    public void loadTransformationMatrix(Matrix4f matrix) {
        loadMatrix(location_transformationMatrix, matrix);
    }

    public void loadProjectionMatrix(Matrix4f projection) {
        loadMatrix(location_projectionMatrix, projection);
    }

    public void loadViewMatrix(Camera camera) {
        Matrix4f viewMatrix = Maths.createViewMatrix(camera);
        loadMatrix(location_viewMatrix, viewMatrix);
    }

    public void loadSkyColour(Vector3f colour) {
        load3DVector(location_skyColour, colour);
    }

    public void loadTintColour(Vector3f colour) {
        load3DVector(location_tintColour, colour);
    }

    public void loadShine(float damper, float reflectivity) {
        loadFloat(location_shineDamper, damper);
        loadFloat(location_reflectivity, reflectivity);
    }

    public void loadUsesFakeLighting(boolean usesFakeLighting) {
        loadBoolean(location_usesFakeLighting, usesFakeLighting);
    }

    public void loadTextureAtlasSize(int size) {
        loadInt(location_textureAtlasSize, size);
    }

    public void loadTextureOffsets(Vector2f offsets) {
        load2DVector(location_textureOffsets, offsets);
    }

    public void loadClippingPlane(Vector4f plane) {
        load4DVector(location_clippingPlane, plane);
    }
}
