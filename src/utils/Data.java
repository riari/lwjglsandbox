package utils;

import java.util.List;

public class Data {
    public static float[] floatListToArray(List<Float> floats) {
        float[] array = new float[floats.size()];
        for (int i = 0; i < array.length; i++) {
            array[i] = floats.get(i);
        }
        return array;
    }
}
