package utils;

import entities.Camera;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import terrain.TerrainTile;
import terrain.TerrainTileSet;

public class MousePicker {
    private static final int RECURSION_COUNT = 200;
    private static final float RAY_RANGE = 600;

    private Vector3f currentRay = new Vector3f();

    private Camera camera;
    private Matrix4f projectionMatrix;
    private Matrix4f viewMatrix;

    private TerrainTileSet terrainTileSet;
    private Vector3f currentTerrainPoint;

    public MousePicker(Camera camera, Matrix4f projectionMatrix, TerrainTileSet terrainTileSet) {
        this.camera = camera;
        this.projectionMatrix = projectionMatrix;
        this.viewMatrix = Maths.createViewMatrix(camera);
        this.terrainTileSet = terrainTileSet;
    }

    public Vector3f getCurrentRay() {
        return currentRay;
    }

    public Vector3f getCurrentTerrainPoint() {
        return currentTerrainPoint;
    }

    public void update() {
        viewMatrix = Maths.createViewMatrix(camera);
        currentRay = calculateMouseRay();
        currentTerrainPoint = (intersectionInRange(0, RAY_RANGE, currentRay))
                ? binarySearch(0, 0, RAY_RANGE, currentRay) : null;
    }

    private Vector3f calculateMouseRay() {
        float mouseX = Mouse.getX();
        float mouseY = Mouse.getY();
        Vector2f ndc = getNormalisedDeviceCoords(mouseX, mouseY);
        Vector4f clipCoords = new Vector4f(ndc.x, ndc.y, -1f, 1f);
        Vector4f eyeCoords = getEyeCoords(clipCoords);
        return getWorldCoords(eyeCoords);

    }

    private Vector2f getNormalisedDeviceCoords(float mouseX, float mouseY) {
        float x = (2f * mouseX) / Display.getWidth() - 1f;
        float y = (2f * mouseY) / Display.getHeight() - 1f;
        return new Vector2f(x, y);
    }

    private Vector4f getEyeCoords(Vector4f clipCoords) {
        Matrix4f invertedProjection = Matrix4f.invert(projectionMatrix, null);
        Vector4f eyeCoords = Matrix4f.transform(invertedProjection, clipCoords, null);
        return new Vector4f(eyeCoords.x, eyeCoords.y, -1f, 0f);
    }

    private Vector3f getWorldCoords(Vector4f eyeCoords) {
        Matrix4f invertedView = Matrix4f.invert(viewMatrix, null);
        Vector4f rayWorld = Matrix4f.transform(invertedView, eyeCoords, null);
        Vector3f mouseRay = new Vector3f(rayWorld.x, rayWorld.y, rayWorld.z);
        mouseRay.normalise();
        return mouseRay;
    }
    private Vector3f getPointOnRay(Vector3f ray, float distance) {
        Vector3f camPos = camera.getPosition();
        Vector3f start = new Vector3f(camPos.x, camPos.y, camPos.z);
        Vector3f scaledRay = new Vector3f(ray.x * distance, ray.y * distance, ray.z * distance);
        return Vector3f.add(start, scaledRay, null);
    }

    private Vector3f binarySearch(int count, float start, float finish, Vector3f ray) {
        float half = start + ((finish - start) / 2f);

        if (count >= RECURSION_COUNT) {
            Vector3f endPoint = getPointOnRay(ray, half);
            TerrainTile terrainTile = getTerrain(endPoint.getX(), endPoint.getZ());
            return (terrainTile != null) ? endPoint : null;
        }

        return intersectionInRange(start, half, ray)
                ? binarySearch(count + 1, start, half, ray)
                : binarySearch(count + 1, half, finish, ray);
    }

    private boolean intersectionInRange(float start, float finish, Vector3f ray) {
        Vector3f startPoint = getPointOnRay(ray, start);
        Vector3f endPoint = getPointOnRay(ray, finish);
        return (!isPointUnderground(startPoint) && isPointUnderground(endPoint));
    }

    private boolean isPointUnderground(Vector3f point) {
        float height = terrainTileSet.getHeightAt(point.getX(), point.getZ());
        return (point.y < height);
    }

    private TerrainTile getTerrain(float worldX, float worldZ) {
        return terrainTileSet.getTileAt(worldX, worldZ);
    }
}
