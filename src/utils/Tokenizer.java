package utils;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Encapsulates a simple text parser, that parses a {@code String} and
 * returns the text as a list of tokens.
 * <p>
 * Regex is used in this parser for convenience and to make it re-usable
 * and extensible, but this could easily be removed to tailor the tokenizer
 * to more specific needs.
 * </p>
 * <p>
 * Parsing is performed by matching regex patterns against the start of the
 * {@code String} and creating a {@code Token} of the matched text. The
 * matched text is removed from the front of the {@code String} and the
 * process is repeated until the entire {@code String} has been parsed and
 * broken down into tokens.
 * </p>
 * <p>
 * Matches are returns as a list of {@code Token}. Each {@code Token} will
 * contain the token and text matched against.
 * </p>
 * <p>
 * If no matching token is found a {@code RunTimeException} is thrown.
 * </p>
 */
public class Tokenizer {
    /**
     * A key/value pair representing a matching pattern in the parsed
     * {@code String}. {@code key} contains the token and {@code value}
     * contains the text matched against.
     */
    public class Token extends Pair<Integer, String> {
        public Token(Integer key, String value) {
            super(key, value);
        }
    }

    /**
     * Hash map of patterns and tokens. Patterns must be unique, but
     * multiple patterns can return the same token.
     * <p>
     * Order matters. {@code LinkedHashMap} will retain the order tokens
     * were added to this tokenizer and tokens will be matched based on
     * their order in this {@code LinkedHashMap}.
     */
    private LinkedHashMap<Pattern, Integer> tokens;  // LinkedHashMap keeps order

    /**
     * Create a new empty tokenizer.
     * <p>
     * Use {@code addToken} to add tokens to this tokenizer.
     * </p>
     */
    public Tokenizer() {
        this.tokens = new LinkedHashMap<>();
    }

    /**
     * Add a pattern/token pair to this tokenizer.
     * <p>
     * Note: The {@code regex} is compiled and wrapped in {@code ^(...)}.
     * </p>
     */
    public void addToken(final String regex, final int token) {
        this.tokens.put(Pattern.compile("^(" + regex + ")"), token);
    }

    /**
     * Parse the given {@code String} and return a list of {@code Token}
     * of all matching tokens in the {@code String}.
     *
     * @throws RuntimeException if no matching token found for an element
     *         within the {@String}.
     */
    public ArrayList<Token> tokenize(final String text) throws RuntimeException {
        ArrayList<Token> tokens = new ArrayList<>();

        String s = text;

        // Loop through the string. Match string against each regex pattern
        // in turn. Create a token of matching text, remove match from front
        // of string and repeat.
        parseLoop:
        while (!s.isEmpty()) {
            for (HashMap.Entry token : this.tokens.entrySet()) {
                Matcher m = ((Pattern) token.getKey()).matcher(s);
                if (m.find()) {
                    tokens.add(new Token((Integer) token.getValue(), m.group()));
                    s = m.replaceFirst("");
                    continue parseLoop;
                }
            }
            // TODO: Proper error handling?
            throw new RuntimeException();
        }

        return tokens;
    }
}
