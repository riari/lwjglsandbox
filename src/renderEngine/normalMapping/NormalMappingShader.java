package renderEngine.normalMapping;

import java.util.List;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import entities.Light;
import shaders.ShaderProgram;

public class NormalMappingShader extends ShaderProgram {
	private static final int MAX_LIGHTS = 5;
	
	private static final String VERTEX_FILE = "NormalMapVS";
	private static final String FRAGMENT_FILE = "NormalMapFS";
	
	private int location_transformationMatrix;
	private int location_projectionMatrix;
	private int location_viewMatrix;
	private int[] location_lightPositionEyeSpace;
	private int[] location_lightColour;
	private int[] location_attenuation;
	private int location_shineDamper;
	private int location_reflectivity;
	private int location_skyColour;
	private int location_atlasSize;
	private int location_offset;
	private int location_plane;
	private int location_modelTexture;
	private int location_normalMap;

	public NormalMappingShader() {
		super(VERTEX_FILE, FRAGMENT_FILE);
	}

	@Override
	protected void bindAttributes() {
		bindAttribute(0, "position");
		bindAttribute(1, "textureCoordinates");
		bindAttribute(2, "normal");
		bindAttribute(3, "tangent");
	}

	@Override
	protected void getAllUniformLocations() {
		location_transformationMatrix = getUniformLocation("transformationMatrix");
		location_projectionMatrix = getUniformLocation("projectionMatrix");
		location_viewMatrix = getUniformLocation("viewMatrix");
		location_shineDamper = getUniformLocation("shineDamper");
		location_reflectivity = getUniformLocation("reflectivity");
		location_skyColour = getUniformLocation("skyColour");
		location_atlasSize = getUniformLocation("atlasSize");
		location_offset = getUniformLocation("offset");
		location_plane = getUniformLocation("plane");
		location_modelTexture = getUniformLocation("modelTexture");
		location_normalMap = getUniformLocation("normalMap");
		
		location_lightPositionEyeSpace = new int[MAX_LIGHTS];
		location_lightColour = new int[MAX_LIGHTS];
		location_attenuation = new int[MAX_LIGHTS];

		for (int i = 0; i < MAX_LIGHTS; i++) {
			location_lightPositionEyeSpace[i] = getUniformLocation("lightPositionEyeSpace[" + i + "]");
			location_lightColour[i] = getUniformLocation("lightColour[" + i + "]");
			location_attenuation[i] = getUniformLocation("attenuation[" + i + "]");
		}
	}
	
	protected void connectTextureUnits() {
		loadInt(location_modelTexture, 0);
		loadInt(location_normalMap, 1);
	}
	
	protected void loadClipPlane(Vector4f plane) {
		load4DVector(location_plane, plane);
	}
	
	protected void loadAtlasSize(int size) {
		loadFloat(location_atlasSize, size);
	}
	
	protected void loadOffset(Vector2f offsets) {
		load2DVector(location_offset, offsets);
	}
	
	protected void loadSkyColour(float r, float g, float b) {
		load3DVector(location_skyColour, new Vector3f(r, g, b));
	}
	
	protected void loadShineVariables(float damper,float reflectivity) {
		loadFloat(location_shineDamper, damper);
		loadFloat(location_reflectivity, reflectivity);
	}
	
	protected void loadTransformationMatrix(Matrix4f matrix) {
		loadMatrix(location_transformationMatrix, matrix);
	}
	
	protected void loadLights(List<Light> lights, Matrix4f viewMatrix) {
		for (int i = 0;i < MAX_LIGHTS; i++) {
			if (i < lights.size()) {
				load3DVector(location_lightPositionEyeSpace[i], getEyeSpacePosition(lights.get(i), viewMatrix));
				load3DVector(location_lightColour[i], lights.get(i).getColour());
				load3DVector(location_attenuation[i], lights.get(i).getAttenuation());
			}else{
				load3DVector(location_lightPositionEyeSpace[i], new Vector3f(0, 0, 0));
				load3DVector(location_lightColour[i], new Vector3f(0, 0, 0));
				load3DVector(location_attenuation[i], new Vector3f(1, 0, 0));
			}
		}
	}
	
	protected void loadViewMatrix(Matrix4f viewMatrix) {
		loadMatrix(location_viewMatrix, viewMatrix);
	}
	
	protected void loadProjectionMatrix(Matrix4f projection) {
		loadMatrix(location_projectionMatrix, projection);
	}
	
	private Vector3f getEyeSpacePosition(Light light, Matrix4f viewMatrix) {
		Vector3f position = light.getPosition();
		Vector4f eyeSpacePos = new Vector4f(position.x,position.y, position.z, 1f);
		Matrix4f.transform(viewMatrix, eyeSpacePos, eyeSpacePos);
		return new Vector3f(eyeSpacePos);
	}
}
