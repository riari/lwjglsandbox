package renderEngine;

import entities.Camera;
import entities.Entity;
import entities.Light;
import models.TexturedModel;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import renderEngine.normalMapping.NormalMappingRenderer;
import shaders.StaticShader;
import shadows.ShadowBox;
import terrain.TerrainRenderer;
import terrain.TerrainShader;
import shadows.ShadowMapMasterRenderer;
import skybox.SkyboxRenderer;
import terrain.TerrainTile;
import terrain.TerrainTileSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MasterRenderer {
    public static final float FOV = 70;
    public static final float NEAR_PLANE = 0.1f;
    public static final float FAR_PLANE = 1000f;

    public static final float RED = 0.11f;
    public static final float GREEN = 0.0f;
    public static final float BLUE = 0.52f;

    private Matrix4f projectionMatrix;

    private StaticShader shader = new StaticShader();
    private EntityRenderer renderer;

    private TerrainRenderer terrainRenderer;
    private TerrainShader terrainShader = new TerrainShader();

    private NormalMappingRenderer normalMapRenderer;
    private ShadowMapMasterRenderer shadowMapRenderer;

    private SkyboxRenderer skyboxRenderer;

    private Map<TexturedModel, List<Entity>> entities = new HashMap<>();
    private Map<TexturedModel, List<Entity>> normalMapEntities = new HashMap<>();
    private List<TerrainTile> terrainTiles = new ArrayList<>();

    public MasterRenderer(Loader loader, Camera camera) {
        enableCulling();
        createProjectionMatrix();
        renderer = new EntityRenderer(shader, projectionMatrix);
        terrainRenderer = new TerrainRenderer(terrainShader, projectionMatrix);
        normalMapRenderer = new NormalMappingRenderer(projectionMatrix);
        shadowMapRenderer = new ShadowMapMasterRenderer(camera);
        skyboxRenderer = new SkyboxRenderer(loader, projectionMatrix);
    }

    public Matrix4f getProjectionMatrix() {
        return projectionMatrix;
    }

    public static void enableCulling() {
        GL11.glEnable(GL11.GL_CULL_FACE);
        GL11.glCullFace(GL11.GL_BACK);
    }

    public static void disableCulling() {
        GL11.glDisable(GL11.GL_CULL_FACE);
    }

    public void renderScene(List<Entity> entities, List<Entity> normalEntities, TerrainTileSet terrain, List<Light> lights, Camera camera, Vector4f clippingPlane) {
        for (TerrainTile terrainTile : terrain.toList()) {
            processTerrain(terrainTile);
        }

        for (Entity entity : entities) {
            processEntity(entity);
        }

        for (Entity entity : normalEntities) {
            processNormalMapEntity(entity);
        }

        render(lights, camera, clippingPlane);
    }

    public void render(List<Light> lights, Camera camera, Vector4f clippingPlane) {
        prepare();

        Vector3f skyColour = new Vector3f(RED, GREEN, BLUE);

        shader.start();
        shader.loadClippingPlane(clippingPlane);
        shader.loadSkyColour(skyColour);
        shader.loadLights(lights);
        shader.loadViewMatrix(camera);
        renderer.render(entities);
        shader.stop();

        normalMapRenderer.render(normalMapEntities, clippingPlane, lights, camera);

        terrainShader.start();
        terrainShader.loadClippingPlane(clippingPlane);
        terrainShader.loadSkyColour(skyColour);
        terrainShader.loadLights(lights);
        terrainShader.loadViewMatrix(camera);
        terrainShader.loadShadowDistance(ShadowBox.SHADOW_DISTANCE);
        terrainShader.loadShadowMapSize(ShadowMapMasterRenderer.SHADOW_MAP_SIZE);
        terrainRenderer.render(terrainTiles, shadowMapRenderer.getToShadowMapSpaceMatrix());
        terrainShader.stop();

        skyboxRenderer.render(camera, skyColour);

        entities.clear();
        normalMapEntities.clear();
        terrainTiles.clear();
    }

    public void prepare() {
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
        GL11.glClearColor(RED, GREEN, BLUE, 1);
        GL13.glActiveTexture(GL13.GL_TEXTURE5);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, getShadowMapTexture());
    }

    public void processEntity(Entity entity) {
        TexturedModel entityModel = entity.getModel();
        List<Entity> batch = entities.get(entityModel);

        if (batch != null) {
            batch.add(entity);
        } else {
            List<Entity> newBatch = new ArrayList<Entity>();
            newBatch.add(entity);
            entities.put(entityModel, newBatch);
        }
    }

    public void processNormalMapEntity(Entity entity) {
        TexturedModel entityModel = entity.getModel();
        List<Entity> batch = normalMapEntities.get(entityModel);

        if (batch != null) {
            batch.add(entity);
        } else {
            List<Entity> newBatch = new ArrayList<Entity>();
            newBatch.add(entity);
            normalMapEntities.put(entityModel, newBatch);
        }
    }

    public void processTerrain(TerrainTile terrainTile) {
        terrainTiles.add(terrainTile);
    }

    public void createProjectionMatrix() {
        float aspectRatio = (float) Display.getWidth() / (float) Display.getHeight();
        float yScale = (float) (1f / Math.tan(Math.toRadians(FOV / 2f)));
        float xScale = yScale / aspectRatio;
        float frustumLength = FAR_PLANE - NEAR_PLANE;

        projectionMatrix = new Matrix4f();
        projectionMatrix.m00 = xScale;
        projectionMatrix.m11 = yScale;
        projectionMatrix.m22 = -((FAR_PLANE + NEAR_PLANE) / frustumLength);
        projectionMatrix.m23 = -1;
        projectionMatrix.m32 = -((2 * NEAR_PLANE * FAR_PLANE) / frustumLength);
        projectionMatrix.m33 = 0;
    }

    public void renderShadowMap(List<Entity> entityList, Light sun) {
        for (Entity entity : entityList) {
            processEntity(entity);
        }

        shadowMapRenderer.render(entities, sun);
        entities.clear();
    }

    public int getShadowMapTexture() {
        return shadowMapRenderer.getShadowMap();
    }

    public void cleanUp() {
        shader.cleanUp();
        terrainShader.cleanUp();
        normalMapRenderer.cleanUp();
        shadowMapRenderer.cleanUp();
    }
}
