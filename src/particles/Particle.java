package particles;

import entities.Camera;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import renderEngine.DisplayManager;

public class Particle {
    private static final float GRAVITY = -50;

    private ParticleTexture texture;

    private Vector3f position;
    private Vector3f velocity;
    private float gravityEffect;
    private float lifetime;
    private float rotation;
    private float scale;

    private Vector2f textureOffset1 = new Vector2f();
    private Vector2f textureOffset2 = new Vector2f();
    private float blendFactor;

    private float distance;
    private float elapsedTime = 0;

    public Particle(ParticleTexture texture, Vector3f position, Vector3f velocity, float gravityEffect, float lifetime, float rotation, float scale) {
        this.texture = texture;
        this.position = position;
        this.velocity = velocity;
        this.gravityEffect = gravityEffect;
        this.lifetime = lifetime;
        this.rotation = rotation;
        this.scale = scale;
        ParticleManager.addParticle(this);
    }

    public ParticleTexture getTexture() {
        return texture;
    }

    public Vector3f getPosition() {
        return position;
    }

    public float getRotation() {
        return rotation;
    }

    public float getScale() {
        return scale;
    }

    public Vector2f getTextureOffset1() {
        return textureOffset1;
    }

    public Vector2f getTextureOffset2() {
        return textureOffset2;
    }

    public float getBlendFactor() {
        return blendFactor;
    }

    public float getDistance() {
        return distance;
    }

    protected boolean update(Camera camera) {
        velocity.y += GRAVITY * gravityEffect * DisplayManager.getFrameTimeSeconds();
        Vector3f change = new Vector3f(velocity);
        change.scale(DisplayManager.getFrameTimeSeconds());
        Vector3f.add(change, position, position);
        distance = Vector3f.sub(camera.getPosition(), position, null).lengthSquared();
        updateTextureCoordInfo();
        elapsedTime += DisplayManager.getFrameTimeSeconds();
        return elapsedTime < lifetime;
    }

    private void updateTextureCoordInfo() {
        float lifeFactor = elapsedTime / lifetime;
        int stageCount = texture.getAtlasSize() * texture.getAtlasSize();
        float atlasProgression = lifeFactor * stageCount;
        int index1 = (int) Math.floor(atlasProgression);
        int index2 = index1 < stageCount - 1 ? index1 + 1 : index1;
        this.blendFactor = atlasProgression % 1;
        setTextureOffset(textureOffset1, index1);
        setTextureOffset(textureOffset2, index2);
    }

    private void setTextureOffset(Vector2f offset, int index) {
        int column = index % texture.getAtlasSize();
        int row = index / texture.getAtlasSize();
        offset.x = (float) column / texture.getAtlasSize();
        offset.y = (float) row / texture.getAtlasSize();
    }
}
