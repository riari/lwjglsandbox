package particles;

public class ParticleTexture {
    private int textureID;
    private int atlasSize;
    private boolean useAdditiveBlending;

    public ParticleTexture(int textureID, int atlasSize) {
        this(textureID, atlasSize, false);
    }

    public ParticleTexture(int textureID, int atlasSize, boolean useAdditiveBlending) {
        this.textureID = textureID;
        this.atlasSize = atlasSize;
        this.useAdditiveBlending = useAdditiveBlending;
    }

    public int getTextureID() {
        return textureID;
    }

    public int getAtlasSize() {
        return atlasSize;
    }

    public boolean usesAdditiveBlending() {
        return useAdditiveBlending;
    }
}
