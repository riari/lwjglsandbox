package textures;

import org.newdawn.slick.opengl.Texture;

public class TerrainTexture {
    private int textureID;

    public TerrainTexture(Texture texture) {
        this.textureID = texture.getTextureID();
    }

    public int getTextureID() {
        return textureID;
    }
}
