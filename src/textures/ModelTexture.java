package textures;

import org.lwjgl.util.vector.Vector3f;
import org.newdawn.slick.opengl.Texture;

public class ModelTexture {
    private int textureID;
    private int normalMap;

    private float shineDamper = 1;
    private float reflectivity = 0;
    private Vector3f tint = new Vector3f(0, 0, 0);

    private boolean hasTransparency = false;
    private boolean usesFakeLighting = false;

    private int atlasSize = 1;

    public ModelTexture(Texture texture) {
        this.textureID = texture.getTextureID();
    }

    public int getID() {
        return textureID;
    }

    public int getNormalMap() {
        return normalMap;
    }

    public void setNormalMap(int normalMap) {
        this.normalMap = normalMap;
    }

    public float getShineDamper() {
        return shineDamper;
    }

    public void setShineDamper(float shineDamper) {
        this.shineDamper = shineDamper;
    }

    public float getReflectivity() {
        return reflectivity;
    }

    public void setReflectivity(float reflectivity) {
        this.reflectivity = reflectivity;
    }

    public Vector3f getTint() {
        return tint;
    }

    public void setTint(Vector3f tint) {
        this.tint = tint;
    }

    public boolean hasTransparency() {
        return hasTransparency;
    }

    public void setHasTransparency(boolean hasTransparency) {
        this.hasTransparency = hasTransparency;
    }

    public boolean isUsingFakeLighting() {
        return usesFakeLighting;
    }

    public void setUsesFakeLighting(boolean usesFakeLighting) {
        this.usesFakeLighting = usesFakeLighting;
    }

    public int getAtlasSize() {
        return atlasSize;
    }

    public void setAtlasSize(int atlasSize) {
        this.atlasSize = atlasSize;
    }
}
